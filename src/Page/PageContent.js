import React from "react";
import "./PageContent.css";

export default function PageContent({children}) {

    return <div className="PageContent">
        {children}
    </div>
}

