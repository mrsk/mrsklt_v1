import React from "react";
import "./PageTitle.css";

export default function PageTitle({children}) {

    return <div className="PageTitle">
        {children}
    </div>
}

