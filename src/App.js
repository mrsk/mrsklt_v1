import React from "react";
import useHashRoute from "./effects/useHashRoute.js";
import useScreenWidth from "./effects/useScreenWidth.js";
import HashRouteController from "./HashRouteController.js";
import { DEFAULT_ROUTE, PAGE_COMPONENTS, PAGE_LINKS } from "./Pages/Pages.js";
import HashLink from "./HashLink/HashLink.js";

const SMALL_SCREEN_WIDTH = 600;

export default function App() {
    let width = useScreenWidth();
    if (width < SMALL_SCREEN_WIDTH) {
        return AppSmallScreen();
    } else {
        return AppNormalScreen();
    }
}

function AppSmallScreen() {
    return <div style={{
        display: "flex",
        flexDirection: "column",
        padding: "10px 10px 10px 10px"
    }}>
        <PageLinks/>
        <br/>
        <SelectedPage/>
    </div>
}

function AppNormalScreen() {
    return <div style={{ display: "flex", flexDirection: "column" }}>
        <HeaderNormalScreen/>
        <MiddleContentNormalScreen/>
        <FooterNormalScreen/>
    </div>
}

function HeaderNormalScreen() {
    return <div style={{ flexBasis: "100px" }}/>
}

function MiddleContentNormalScreen() {
    return <div style={{
        display: "flex",
        flexDirection: "row", 
    }}>
        <LeftPanel/>
        <RightPanel/>
    </div>
}

function LeftPanel() {
    return <div style={{
        flexBasis: "600px",
        flexGrow: 2,
        flexShrink: 1,
        display: "flex",
        flexDirection: "row",
    }}>
        <div style={{ flexBasis: "100px", flexGrow: 1, minWidth: "10px" }}/>
        <div style={{ flexBasis: "400px", flexGrow: 2 }}>
            <SelectedPage/>
        </div>
        <div style={{ flexBasis: "100px", flexGrow: 1, minWidth: "30px" }}/>
    </div>
}

function SelectedPage() {
    let routeParts = useHashRoute(DEFAULT_ROUTE);
    let parsedRoute = routeParts[0];
    let unparsedRoute = routeParts.slice(1);
    let SelectedPageComponent = PAGE_COMPONENTS[parsedRoute];
    if (SelectedPageComponent == null) {
        HashRouteController.setHashRoute(DEFAULT_ROUTE);
        SelectedPageComponent = PAGE_COMPONENTS[DEFAULT_ROUTE];
    }
    return <SelectedPageComponent baseRoute={parsedRoute} unparsedRoute={unparsedRoute}/>
}

function RightPanel() {
    return <div style={{
        flexBasis: "200px",
        flexGrow: 1,
        flexShrink: 0,
        marginTop: "50px"
    }}>
        <PageLinks/>
    </div>
}

function PageLinks() {
    return PAGE_LINKS.map(l => 
        <HashLink href={l.hash} key={l.hash}>
            {l.name}
        </HashLink>
    );
}

function FooterNormalScreen() {
    return <div style={{ flexBasis: "100px" }}/>
}

