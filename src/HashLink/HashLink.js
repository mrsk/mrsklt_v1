import React from "react";
import "./HashLink.css";

export default function HashLink({children, href}) {

    return <a 
        style={{ display: "block" }}
        className="HashLink"
        href={href}
    >
        {children}
    </a>
}



