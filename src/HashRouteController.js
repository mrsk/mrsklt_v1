import urlJoin from "proper-url-join";

class HashRouteController {

    constructor() {
        this.setHashRoute = this.setHashRoute.bind(this);
        this.addListener = this.addListener.bind(this);
        this.removeListener = this.removeListener.bind(this);
        this.splitRoute = this.splitRoute.bind(this);
        this.joinRoute = this.joinRoute.bind(this);
    }

    setHashRoute(hashRoute) {
        window.location.hash = hashRoute;
    }

    addListener(listener) {
        window.addEventListener("hashchange", listener);
    }

    removeListener(listener) {
        window.removeEventListener("hashchange", listener);
    }

    splitRoute(defaultRoute) {
        const result = window.location.hash.split("/");
        if (result == null || result.length === 0) {
            return [defaultRoute];
        }
        return result;
    }
    
    joinRoute() {
        // arguments is not iterable on some browsers
        let args = [];
        let argsLen = arguments.length;
        for (let i = 0; i < argsLen; i++) {
            let arg = arguments[i];
            if (arg == null) {
                continue;
            }
            
            let argType = typeof arg;
            if (argType === "string" || argType === "number") {
                args.push(arg);
            } else {
                throw new Error("bad typeof arg");
            }
            
        }

        return urlJoin(...args, { leadingSlash: false })
    }

}

export default new HashRouteController();
