import { useState, useEffect } from "react";
import HashRouteController from "../HashRouteController";

export default function useHashRoute(defaultRoute) {
    let [routeParts, setRouteParts] = useState(HashRouteController.splitRoute(defaultRoute))
    useEffect(() => {
        function onhashchange() {
            setRouteParts(HashRouteController.splitRoute(defaultRoute));
        }
        HashRouteController.addListener(onhashchange);
        return () => {
            HashRouteController.removeListener(onhashchange); 
        }
    }, []);
    return routeParts;
}
