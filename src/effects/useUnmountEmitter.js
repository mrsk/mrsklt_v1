import { useRef, useEffect } from "react";

export default function useUnmountEmitter() {
    let unmountEmitter = useRef(null);
    if (unmountEmitter.current == null) {
        unmountEmitter.current = new UnmountEmitter();
    }
    useEffect(() => () => unmountEmitter.current.emit(), []);
    return unmountEmitter.current;
}

class UnmountEmitter {

    constructor() {
        this.addListenerOnce = this.addListenerOnce.bind(this);
        this.emit = this.emit.bind(this);
        this.isMounted = true;
        this.listeners = new Set();
        this.abortController = new AbortController();
    }

    addListenerOnce(listener) {
        this.listeners.add(listener);
    }

    emit() {
        this.isMounted = false;
        let len = this.listeners.length;
        for (let i = 0; i < len; i++) {
            this.listeners[i]();
        }
        this.listeners.clear();
        this.abortController.abort()
    }

}
