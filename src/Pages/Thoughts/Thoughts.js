
import HashRouteController from "../../HashRouteController.js";
import ExistanceThought from "./ExistanceThought.js";
import PurposeOfLifeThougt from "./PurposeOfLifeThought.js";

const THOUGHTS_ROUTES = {
    death: "Death",
    purpose: "PurposeOfLife",
    godels: "GodelsIncompletenessTheorems",
    projects: "ProjectManagement",
    existance: "Existance",
}


const THOUGHTS_COMPONENTS = {};
THOUGHTS_COMPONENTS[THOUGHTS_ROUTES.existance] = ExistanceThought;
THOUGHTS_COMPONENTS[THOUGHTS_ROUTES.purpose] = PurposeOfLifeThougt;

const THOUGHTS_LINKS = [
    //{
    //    name: "Purpose of Life",
    //    hash: baseRoute => HashRouteController.joinRoute(baseRoute, THOUGHTS_ROUTES.purpose)
    //}, 
    {
        name: "Existance",
        hash: baseRoute => HashRouteController.joinRoute(baseRoute, THOUGHTS_ROUTES.existance)
    }
];

export {
    THOUGHTS_COMPONENTS,
    THOUGHTS_LINKS,
};
