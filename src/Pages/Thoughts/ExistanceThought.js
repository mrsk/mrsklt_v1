import * as React from "react";
import HashLink from "../../HashLink/HashLink.js";
import PageTitle from "../../Page/PageTitle.js";
import PageContent from "../../Page/PageContent.js";

export default function ExistanceThought({baseRoute, unparsedRoute, backRoute}) {
    return <div>
        <HashLink href={backRoute}>{"↩ back"}</HashLink>
        <PageTitle>Why does anything exist at all?</PageTitle>
        <PageContent>

        The short answer is I don’t know. But I have some beliefs about it 
        and I am always trying to stick them together in a reasonable way.
        
        <br/>
        <br/>

        <b>How do I use the word "exists"?</b>
        <br/>
        Any existing object has a domain which enables its existence. 
        This domain is the reason why objects can exist. Stating that some 
        object exists can only make sense when the domain is defined. 
        In many cases, the domain is implicit and causes some confusion.
        
        <br/>
        <br/>

        <b>The boundary between nothing and everything.</b>
        <br/>
        A domain can also be seen as an inescapable scope or a boundary. 
        And beyond the boundary of everything, there is nothing. And outside 
        of nothing, there is everything else. Nothing must be coexisting 
        with everything bounded and defined by each other.
        
        <br/>
        <br/>

        <b>What is this everything?</b>
        <br/>
        It’s the big bowl of boiling soup of randomness. Strange things 
        can emerge out of it. Like some logical patterns, causation, space 
        and time. And all of our physical universe happens to be some 
        part of it.


        <br/>
        <br/>
        You can read more about the topic on this <a 
            href="https://en.wikipedia.org/wiki/Problem_of_why_there_is_anything_at_all"
        >wiki page</a>.
        </PageContent>
    </div>
}
