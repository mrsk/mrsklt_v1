import * as React from "react";
import HashLink from "../../HashLink/HashLink.js";
import PageTitle from "../../Page/PageTitle.js";
import PageContent from "../../Page/PageContent.js";

export default function PurposeOfLifeThought({baseRoute, unparsedRoute, backRoute}) {
    return <div>
        <HashLink href={backRoute}>{"↩ back"}</HashLink>
        <PageTitle>What is purpose of life?</PageTitle>
        <PageContent>
            . . .        
        </PageContent>
    </div>
}
