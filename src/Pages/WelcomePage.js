import React from "react";
import PageContent from "../Page/PageContent.js";
import PageTitle from "../Page/PageTitle.js";

export default function WelcomePage() {
    return <div>
        <PageTitle>
            Welcome
        </PageTitle>
        <PageContent>

            Welcome to this over-engineered, all static, reactjs website.
            It is hosted somewhere sometimes, via Inter Planetary File System (IPFS) network.
            Some larger media files are hosted on <a href="https://pics.mrsk.lt">https://pics.mrsk.lt</a>.
            Publicly trusted TLS certificate for <a href="https://mrsk.lt">mrsk.lt</a> is provided by Cloudflare services.
            You can dirrectly visit this website on <a href="https://gateway.ipfs.io/ipns/mrsk.lt">public IPFS gateway</a>. 
            Source code is available at <a href="https://gitlab.com/mrsk/mrsklt">gitlab</a>.
            To make things worse I am planning to rewrite this website in Rust+wasm using Yew framework.
            
            <br/>
            <br/>

            Any views, opinions or ideas expressed here are my own (they could be out of date though) 
            and do not in any way represent the views of any other entity.

            <br/>
            <br/>

            Enjoy:)

        </PageContent>
    </div>;
}

