import React from "react";
import PageContent from "../Page/PageContent.js";
import PageTitle from "../Page/PageTitle.js";
import {THOUGHTS_COMPONENTS, THOUGHTS_LINKS} from "./Thoughts/Thoughts.js";
import HashLink from "../HashLink/HashLink.js";
import DirectoryPage from "./DirectoryPage.js";

export default function ThoughtsPage({baseRoute, unparsedRoute}) {
    return <DirectoryPage
        rootComponent={Root}
        childComponents={THOUGHTS_COMPONENTS}
        baseRoute={baseRoute}
        unparsedRoute={unparsedRoute}
    />;
}


function Root({baseRoute}) {
    return <div>
        <PageTitle>
            What I think about . . .
        </PageTitle>
        <PageContent>

            <i>
                "In order for him to believe sincerely in eternity, others had to share in this belief, 
                because a belief that no one else shares is called schizophrenia."
            </i>
            <br/>
            ― Victor Pelevin, Homo Zapiens 
            <br/>
            <br/>
            <br/>
            Here you can find some topics which are interesting to me and I have some opinions about.
            <br/>
            Please let me know if I am not being consistent with myself.
            <br/>
            <br/>

            <ThoughtsLinks baseRoute={baseRoute}/>
        </PageContent>
    </div>
}

function ThoughtsLinks({baseRoute}) {
    return THOUGHTS_LINKS.map(thoughtLink => {
        let href = thoughtLink.hash(baseRoute);
        return <HashLink key={href} href={href}>{thoughtLink.name}</HashLink>;
    });
}

