import * as React from "react";
import { useState, useEffect } from "react";
import Lightbox from "react-images";

import PageTitle from "../../Page/PageTitle.js";
import PageContent from "../../Page/PageContent.js";
import HashRouteController from "../../HashRouteController.js";
import useUnmountEmitter from "../../effects/useUnmountEmitter.js";
import HashLink from "../../HashLink/HashLink.js";

export default function NginxFGallery({
    fGalleryUrl, 
    title, 
    baseRoute, 
    unparsedRoute,
    backRoute
}) {
    return <div>
        <HashLink href={backRoute}>{"↩ back to gallery list"}</HashLink>
        <PageTitle>{title}</PageTitle>
        {Content({fGalleryUrl, unparsedRoute, baseRoute})}
    </div>
}

function Content({fGalleryUrl, unparsedRoute, baseRoute}) {
    let {images, imagesLoaded} = useImages(fGalleryUrl);
    let previewIndex = Number.parseInt(Number(unparsedRoute[0]));
    let previewIsOpen = !isNaN(previewIndex);

    if (!imagesLoaded) {
        return "Loading..."; // FIXME create loading component
    }

    if (unparsedRoute[0] && !(previewIndex >= 0 && previewIndex < images.length)) {
        HashRouteController.setHashRoute(baseRoute);
        return "Loading..."; // FIXME create loading component
    }

    return <PageContent>
        {Gallery({images, baseRoute})}
        {Preview({images, previewIndex, previewIsOpen, baseRoute})}
    </PageContent>;
}


function Gallery({images, setPreviewIsOpen, baseRoute}) {
    images = images || [];
    let thumbnails = [];
    let imagesLen = images.length;
    for (let i = 0; i < imagesLen; i++) {
        let {thumbnail} = images[i];
        function onClick() {
            let hashRoute = HashRouteController.joinRoute(baseRoute, i);
            HashRouteController.setHashRoute(hashRoute);
        }

        thumbnails.push(<img 
            key={i} 
            src={thumbnail} 
            style={{cursor: "pointer"}} 
            onClick={onClick}
            alt=""
        />);
    }

    return <div style={{display: "flex", flexWrap: "wrap", flexDirection: "row"}}>
        {thumbnails}
    </div>
}

function Preview({images, previewIndex, previewIsOpen, baseRoute}) {

    images = images.map(i => ({ src: i.original }));
    let len = images.length;

    function onClose() {
        HashRouteController.setHashRoute(baseRoute);
    }

    function onNavigate(deltaIndex) {
        let newPreviewIndex = (previewIndex + deltaIndex) % len;
        if (newPreviewIndex < 0) {
            newPreviewIndex += len;
        }
        let hashRoute = HashRouteController.joinRoute(baseRoute, newPreviewIndex);
        HashRouteController.setHashRoute(hashRoute);
    }

    function onClickPrev() {
        onNavigate(-1);
    }

    function onClickNext() {
        onNavigate(1);
    }

    return <Lightbox 
        images={images}
        isOpen={previewIsOpen}
        onClose={onClose}
        currentImage={previewIndex}
        onClickPrev={onClickPrev}
        onClickNext={onClickNext}
        preloadNextImage={true}
    />
}



let IMAGES_CACHE = {};

function useImages(fGalleryUrl) {

    let initState = {
        images: IMAGES_CACHE[fGalleryUrl] || [],
        imagesLoaded: IMAGES_CACHE[fGalleryUrl] != null
    };

    let [images, setImages] = useState(initState.images);
    let [imagesLoaded, setImagesLoaded] = useState(initState.imagesLoaded);

    let unmountEmitter = useUnmountEmitter();

    useEffect(() => {
        if (!imagesLoaded) {
            fetchImages();
        }
    }, [fGalleryUrl]);

    async function fetchImages() {
        try {
            let dirUrl = HashRouteController.joinRoute(fGalleryUrl, "imgs");
            let response = await fetch(dirUrl, { signal: unmountEmitter.abortController.signal });
            let responseText = await response.text()
            if (!unmountEmitter.isMounted) {
                return;
            }
            let images = extractImages(fGalleryUrl, responseText);
            setImages(images);
            setImagesLoaded(true);
            IMAGES_CACHE[fGalleryUrl] = images;
        } catch (e) {
            // FIXME do something..
            console.error(e);
        }
    }
    return {images, imagesLoaded};
}




function extractImages(fGalleryUrl, responseText) {
    let result = [];
    const lines = responseText.match(/[^\r\n]+/g);
    for (const line of lines || []) {
        const urls = extractUrls(fGalleryUrl, line);
        if (urls != null) {
            result.push(urls);
        }
    }
    return result;
}


// nginx autoindex format:
// <a href="IMG_20180705_114604.jpg">IMG_20180705_114604.jpg</a>                            15-Jul-2018 09:39              349528
function extractUrls(fGalleryUrl, text) {
    if (!text.startsWith("<a href=\"")) {
        return null;
    }

    const urlEndIndex = text.indexOf("\">");
    const imgName = text.slice(9, urlEndIndex);
    const original = HashRouteController.joinRoute(fGalleryUrl, "imgs", imgName);
    const thumbnail = HashRouteController.joinRoute(fGalleryUrl, "thumbs", imgName);

    return {original, thumbnail};
}

