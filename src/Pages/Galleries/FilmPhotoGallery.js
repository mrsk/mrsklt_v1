

import * as React from "react";
import NginxFGallery from "./NginxFGallery.js";

export default function FilmPhotoGallery({baseRoute, unparsedRoute, backRoute}) {
    return <NginxFGallery
        fGalleryUrl="https://pics.mrsk.lt/filmphoto/"
        title="Film photography"
        baseRoute={baseRoute}
        unparsedRoute={unparsedRoute}
        backRoute={backRoute}
    />
}

