

import * as React from "react";
import NginxFGallery from "./NginxFGallery.js";

export default function TravelGallery({baseRoute, unparsedRoute, backRoute}) {
    return <NginxFGallery
        fGalleryUrl="https://pics.mrsk.lt/austria/"
        title="Travel pictures"
        baseRoute={baseRoute}
        unparsedRoute={unparsedRoute}
        backRoute={backRoute}
    />
}
