
import HashRouteController from "../../HashRouteController.js";
import FilmPhotoGallery from "./FilmPhotoGallery.js";
import TravelGallery from "./TravelGallery.js";

const GALLERY_ROUTES = {
    travel: "Travel",
    filmPhoto: "FilmPhoto"
};

const GALLERY_COPMONENTS = {};
GALLERY_COPMONENTS[GALLERY_ROUTES.travel] = TravelGallery;
GALLERY_COPMONENTS[GALLERY_ROUTES.filmPhoto] = FilmPhotoGallery;

const GALLERY_LINKS = [{
        name: "Travel pictures",
        hash: baseRoute => HashRouteController.joinRoute(baseRoute, GALLERY_ROUTES.travel)
    }, {
        name: "Film photography",
        hash: baseRoute => HashRouteController.joinRoute(baseRoute, GALLERY_ROUTES.filmPhoto)
    }
];

export {
    GALLERY_COPMONENTS,
    GALLERY_LINKS,
};
