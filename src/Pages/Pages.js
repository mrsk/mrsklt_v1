import WelcomePage from "./WelcomePage.js";
import AboutMePage from "./AboutMePage.js";
import GalleryPage from "./GalleryPage.js";
import InspirationalPeoplePage from "./InspirationalPeoplePage.js";
import ThoughtsPage from "./ThoughtsPage.js";

const PAGE_ROUTES = {
    welcome: "#Welcome",
    aboutMe: "#AboutMe",
    thoughts: "#WhatIThinkAbout",
    gallery: "#Gallery",
    inspirationalPeople: "#InspirationalPeople"
};

const DEFAULT_ROUTE = PAGE_ROUTES.welcome;

const PAGE_COMPONENTS = {};
PAGE_COMPONENTS[PAGE_ROUTES.welcome] = WelcomePage;
PAGE_COMPONENTS[PAGE_ROUTES.aboutMe] = AboutMePage;
PAGE_COMPONENTS[PAGE_ROUTES.thoughts] = ThoughtsPage;
PAGE_COMPONENTS[PAGE_ROUTES.gallery] = GalleryPage;
PAGE_COMPONENTS[PAGE_ROUTES.inspirationalPeople] = InspirationalPeoplePage;

const PAGE_LINKS = [{
        name: "Welcome!",
        hash: PAGE_ROUTES.welcome
    }, {
        name: "About me",
        hash: PAGE_ROUTES.aboutMe
    }, {
        name: "What I think about..",
        hash: PAGE_ROUTES.thoughts
    }, {
        name: "Gallery",
        hash: PAGE_ROUTES.gallery
    }, {
        name: "Inspirational people",
        hash: PAGE_ROUTES.inspirationalPeople
    },
];

export {
    DEFAULT_ROUTE,
    PAGE_COMPONENTS,
    PAGE_LINKS
};
