import React from "react";
import HashRouteController from "../HashRouteController.js";

export default function DirectoryPage({rootComponent, childComponents, baseRoute, unparsedRoute}) {
    const selectedChild = getSelectedChild({
        baseRoute, 
        unparsedRoute, 
        childComponents

    });
    
    function Root() {
        return rootComponent({baseRoute});
    } 

    if (selectedChild == null) {
        return <Root/>
    }

    return selectedChild;
}

function getSelectedChild({baseRoute, unparsedRoute, childComponents}) {
    let backRoute = baseRoute;
    let parsedRoute = unparsedRoute[0];
    let selectedChildComponent = childComponents[parsedRoute];
    if (selectedChildComponent == null) {
        return null
    }
    baseRoute = HashRouteController.joinRoute(baseRoute, parsedRoute);
    unparsedRoute = unparsedRoute.slice(1);
    function Child() {
        return selectedChildComponent({baseRoute, unparsedRoute, backRoute});
    }
    
    return <Child />
}

