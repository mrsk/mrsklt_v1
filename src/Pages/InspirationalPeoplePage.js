import React from "react";
import PageContent from "../Page/PageContent.js";
import PageTitle from "../Page/PageTitle.js";

export default function InspirationalPeoplePage() {
    return <div>
        <PageTitle>
            Inspirational people
        </PageTitle>
        <PageContent>
            <ul style={{ paddingLeft: "40px" }}>
                <li>Carl Sagan</li>
                <li>Linus Torvalds</li>
                <li>Roger Penrose</li>
                <li>Vi Hart</li>
                <li>Marshall Rosenberg</li>
                <li>Louis Sauzedde</li>
                <li>Tor Eckhoff</li>
                <li>Richard Stallman</li>
                <li>Lucius Annaeus Seneca</li>
                <li>Maurits Cornelis Escher</li>
            </ul>
            I hope they don't mind beeing together on one list:)
        </PageContent>
    </div>;
}
