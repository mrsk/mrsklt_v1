import React from "react";
import PageContent from "../Page/PageContent.js";
import PageTitle from "../Page/PageTitle.js";
import {GALLERY_COPMONENTS, GALLERY_LINKS} from "./Galleries/Galleries.js";
import DirectoryPage from "./DirectoryPage.js";
import HashLink from "../HashLink/HashLink.js";

export default function GalleryPage({baseRoute, unparsedRoute}) {

    return <DirectoryPage
        rootComponent={Root}
        childComponents={GALLERY_COPMONENTS}
        baseRoute={baseRoute}
        unparsedRoute={unparsedRoute}
    />;
}

function Root({baseRoute}) {
    return <div>
        <PageTitle>
            Gallery
        </PageTitle>
        <PageContent>
            <GalleryLinks baseRoute={baseRoute}/>
        </PageContent>
    </div>
}

function GalleryLinks({baseRoute}) {
    // FIXME show sume thumbnails
    return GALLERY_LINKS.map(galleryLink => {
        let href = galleryLink.hash(baseRoute);
        return <HashLink key={href} href={href}>{galleryLink.name}</HashLink>
    });
}

