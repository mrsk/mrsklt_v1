import React from "react";
import PageContent from "../Page/PageContent.js";
import PageTitle from "../Page/PageTitle.js";
import ProfileImage from "./ProfileImage.png";

export default function WelcomePage() {
    return <div>
        <PageTitle>
            About me
        </PageTitle>
        <PageContent>
            <img 
                style={{maxWidth: "100%"}}
                src={ProfileImage}
                alt=""
            />

            <br/>
            <br/>

            Hi, my name is Marius. I am a dreamer / computer scientist / physicist / nature lover.
            I can help you solve or avoid tech problems.

            <br/>
            <br/>

            A few things I like doing:
            <ul style={{ paddingLeft: "40px" }}>
                <li>Cycling</li>
                <li>Folk dancing</li>
                <li>Sailing</li>
                <li>Stargazing</li>
                <li>Climbing mountains</li>
            </ul>

            <br/>
            <br/>
            
            Contact me on <a 
                target="_blank"
                rel="noopener noreferrer"
                href="https://riot.im/app/#/user/@marius_k:matrix.org"
            >matrix.org</a>.

        </PageContent>
    </div>;
}
